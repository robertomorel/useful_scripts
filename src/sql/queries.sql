SELECT * FROM projects p
WHERE p.name like 'Heavy%'
AND id in (
    SELECT ynps.project_id FROM youtube_non_programming_statistics ynps WHERE subscriber_count > 0
);

--SELECT * FROM youtube_non_programming_statistics ynps WHERE subscriber_count > 0;

SELECT * FROM projects p;

SELECT * FROM media_link ml
WHERE ml.`type` = 'twitter';

SELECT * FROM offsite_video ov 
WHERE `type` = 'twitter'
AND id = 457;

SELECT COUNT(DISTINCT(tps.project_id)) from twitter_programming_statistics tps; 

SELECT COUNT(DISTINCT(tps.offsite_video_id)) from twitter_programming_statistics tps; 

SELECT * from twitter_programming_statistics tps
WHERE tps.offsite_video_id = 457;

SELECT COUNT(*) FROM offsite_video ov 
Inner Join twitter_programming_statistics tps
    on ov.id = tps.offsite_video_id 
WHERE `type` = 'twitter';   

select sum(DATEDIFF(now(), p.prelaunch_date)/30) 
from projects p
inner join media_link ml on p.id = ml.project_id
where ml.type='twitter';

select p.* 
from projects p
inner join media_link ml on p.id = ml.project_id
where ml.type='twitter'
and ml.project_id = 230;

SELECT * FROM media_link ml
WHERE ml.`type` = 'twitter';

SELECT id, period_of, project_id, media_link_id, offsite_video_id, view_count, season_number, episode_number
FROM twitter_programming_statistics tps 
WHERE project_id = 431
AND media_link_id = 954
AND offsite_video_id = 2390
ORDER by period_of;

SELECT DISTINCT (period_of)
FROM twitter_programming_statistics tps 
WHERE project_id = 431
AND 
ORDER by period_of;

SELECT c.TABLE_NAME, 
       GROUP_CONCAT(c.COLUMN_NAME) as COLUMN_NAME, 
       'desc' as DESCRIPTION FROM INFORMATION_SCHEMA.`COLUMNS` c
WHERE c.TABLE_SCHEMA = 'vc_client'
GROUP BY c.TABLE_NAME
ORDER BY c.TABLE_NAME;

SELECT * FROM INFORMATION_SCHEMA.`COLUMNS` c
WHERE c.TABLE_SCHEMA = 'vc_client';



select project_id, datediff(now(), max(period_of)) as dateDiff from facebook_programming_statistics group by project_id;






SELECT * FROM media_link ml
WHERE ml.`type` = 'twitter'
AND id = 954;

SELECT id, period_of, project_id, media_link_id, offsite_video_id, view_count, season_number, episode_number
FROM twitter_programming_statistics tps 
WHERE 1 = 1
AND project_id = 431
AND media_link_id = 954
--AND period_of >= NOW() - INTERVAL 2 MONTH
--AND offsite_video_id = 2505
ORDER by period_of, offsite_video_id;

select * from media_link ml where id = 954;
select * from offsite_video ov where ov.offsite_id in ('7_1253432270297284614');
select * from offsite_video where `type` = 'twitter';

SELECT DISTINCT (period_of), offsite_video_id 
FROM twitter_programming_statistics tps 
WHERE 1 = 1
AND project_id = 431
AND media_link_id = 954
--AND period_of >= NOW() - INTERVAL 2 MONTH
--AND offsite_video_id = 2505
ORDER by period_of, offsite_video_id;


SELECT count(DISTINCT (period_of)) 
FROM twitter_programming_statistics tps 
WHERE 1 = 1
AND project_id = 431
AND media_link_id = 954
--AND period_of >= NOW() - INTERVAL 2 MONTH
--AND offsite_video_id = 2505
ORDER by period_of, offsite_video_id;








SELECT id, period_of, project_id , twitter_no_of_tweets, twitter_followers 
FROM twitter_non_programming_statistics tnps 
WHERE twitter_followers > 0
AND project_id = 224
ORDER BY period_of;

SELECT id, period_of, project_id , twitter_no_of_tweets, twitter_followers 
FROM twitter_non_programming_statistics tnps 
WHERE twitter_followers > 0
AND project_id = 181
ORDER BY period_of;

select * from offsite_video where `type` = 'twitter';


select distinct(tps.project_id), ov.link from twitter_programming_statistics tps
inner join offsite_video ov on tps.offsite_video_id = ov.id
where ov.link is not NULL 
and ov.`type` = 'twitter'
order by tps.project_id;


select * from auth_tokens at2 
where project_id = 224;

SELECT * FROM projects p 
where p.id in (186,224,229,453,181,417,203,416,49,408,423,202,412,206,406,415,205,407,226,431,208,441)
and `type` = 'SERIES'
and `type` = 'ONE_OFF';


select project_id, id from seasons s
join (
    select season_id, MAX(episode_number) as max_episode_number from episodes 
    group by season_id
) as season_episodes on season_episodes.season_id = s.id
where s.project_id in (
    select id from projects 
    where id in (186,224,229,453,181,417,203,416,49,408,423,202,412,206,406,415,205,407,226,431,208,447441)
    and `type` = 'SERIES'
)
and season_episodes.max_episode_number >= 7;





SELECT projects.name,
       media_link.type,
       media_link.project_id,
       media_link.auth_tokens_id,
       media_link.id as media_link_id 
from media_link, projects  
where media_link.project_id in 
    (SELECT projects.id 
     from projects, project_fund 
     where projects.type not in ('LEGACY_ONE_OFF', 'LEGACY_SERIES') 
     and projects.id = project_fund.project_id 
     and project_fund.fund_id not in (8,9)) 
and projects.id = media_link.project_id;




select project_id, offsite_video_id, sum(video_views)
from instagram_programming_statistics ips 
where 
    project_id = 431 and
    period_of <= '2021-08-01' and 
    period_of < '2021-09-01'
group by project_id,offsite_video_id 
order by project_id, -sum(video_views);

select *
from instagram_programming_statistics ips 
where project_id = 431

 
select 
    page_positive_feedback_by_type_unique
from facebook_non_programming_statistics fnps
where fnps.project_id = 378;

select 
    json_extract(page_positive_feedback_by_type_unique, '$.link') as link,
    page_positive_feedback_by_type_unique
from facebook_non_programming_statistics fnps
where fnps.project_id = 378;



SELECT 
    SUM(json_extract(page_positive_feedback_by_type_unique, '$.link')) as link,
    SUM(json_extract(page_positive_feedback_by_type_unique, '$.like')) as 'like',
    SUM(json_extract(page_positive_feedback_by_type_unique, '$.comment')) as comment,
    SUM(json_extract(page_positive_feedback_by_type_unique, '$.other')) as other,
    period_of as period_of
FROM facebook_non_programming_statistics 
WHERE project_id = 378  
AND (json_extract(page_positive_feedback_by_type_unique, '$.link') > 0 or 
     json_extract(page_positive_feedback_by_type_unique, '$.like') > 0 or 
     json_extract(page_positive_feedback_by_type_unique, '$.comment') > 0 or 
     json_extract(page_positive_feedback_by_type_unique, '$.other'))
GROUP BY period_of;





select project_id, SUM(followers) from instagram_non_programming_statistics 
where project_id in (
    select id from projects 
    where name in (
        'Dark Cloud: The High Cost of Cyberbullying',
        'Parfaitement Imparfait',
        'Where Oliver Fits'
    )
)
group by project_id;

select project_id, SUM(followers) from instagram_non_programming_statistics 
where project_id = 229
group by project_id;

select id, name from projects 
    where name in (
        'Dark Cloud: The High Cost of Cyberbullying',
        'Parfaitement Imparfait',
        'Where Oliver Fits'
    );