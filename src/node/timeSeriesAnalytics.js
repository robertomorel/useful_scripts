import timeseries from "timeseries-analysis";
import * as fs from "fs";
import * as ss from "simple-statistics";
import groupByTime from "group-by-time";

// import objects from "./utils/objects";

function calculateSum(array, propertyArr) {
  const total = array.reduce((accumulator, object) => {
    const obj = {};
    for (let p of propertyArr) {
      obj[p] = (object[p] || 0) + (accumulator[p] || 0);
    }
    return obj;
  }, {});

  return total;
}

function formatArr(data, propertyArr) {
  const obj = {};
  Object.entries(data).forEach(([key, values]) => {
    obj[key] = calculateSum(values, propertyArr);
  });

  return obj;
}

let instagramData = JSON.parse(
  fs.readFileSync(
    "src/node/mock_data/instagram_programming_statistics_short.json",
    "utf-8"
  )
);

function timeSeriesAnalysis() {
  instagramData = instagramData.map((elem) => {
    return {
      ...elem,
      period_of: new Date(elem.period_of),
    };
  });

  // const t = new timeseries.main(instagramData);

  const t = new timeseries.main(
    timeseries.adapter.fromDB(instagramData, {
      date: "period_of", // Name of the property containing the Date (must be compatible with new Date(date) )
      value: "like_count", // Name of the property containign the value. here we'll use the "close" price.
    })
  );
  console.log(t);

  // MIN
  var min = t.min();
  console.log(min);

  // Max
  var max = t.max();
  console.log(max);

  // Mean (Avegare)
  var mean = t.mean();
  console.log(mean);

  // Standard Deviation
  var stdev = t.stdev();
  console.log(stdev);

  t.ma({
    period: 6,
  });

  var chart_url = t.ma({ period: 14 }).chart({ main: true });
  console.log(chart_url);
}

function simpleStatistics() {
  let groupedByDay = groupByTime(instagramData, "period_of", "day");
  let groupedByWeek = groupByTime(instagramData, "period_of", "week");
  let groupedByMonth = groupByTime(instagramData, "period_of", "month");

  console.log(groupedByDay);
  console.log(groupedByWeek);
  console.log(groupedByMonth);

  groupedByDay = formatArr(groupedByMonth, [
    "like_count",
    "reach",
    "saved",
    "comments_count",
  ]);

  console.log(groupedByWeek);
}

(() => {
  // timeSeriesAnalysis();
  simpleStatistics();
})();

/**
 * Documents
 *
 * https://www.npmjs.com/package/stats-lite
 * https://simplestatistics.org/docs/#min
 *
 * https://www.npmjs.com/package/simple-statistics
 * https://github.com/simple-statistics/simple-statistics
 *
 * https://www.npmjs.com/package/group-by-time
 * https://github.com/Techwraith/group-by-time
 *
 * https://linuxhint.com/convert-timestamp-to-date-format-javascript/#:~:text=To%20convert%20timestamp%20to%20date%20format%20in%20JavaScript%2C%20apply%20the,and%20date%20and%20display%20them.
 */
