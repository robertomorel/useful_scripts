const express = require('express');
const app = express();
//const fetch = require('node-fetch');
const cookieParser = require('cookie-parser');
const cors = require('cors');

app.use(cookieParser());
app.use(cors());
app.listen(process.env.PORT || 8080);

const CLIENT_KEY = 'awjbt82smah4sh2o' // this value can be found in app's developer portal
const CLIENT_SECRET = '60f9bde5d59a9480666996bdf744b2c8' // this value can be found in app's developer portal
const SERVER_ENDPOINT_REDIRECT = 'http://localhost:8080/verify/tiktok/';

const CLOSE_WINDOW = `<script>
                         window.close();
                      </script>`;

app.get('/oauth', async (req, res) => {
    const csrfState = Math.random().toString(36).substring(7);
    res.cookie('csrfState', csrfState, { maxAge: 60000 });

    let url = 'https://open-api.tiktok.com/platform/oauth/connect';

    url += `?client_key=${CLIENT_KEY}`;
    url += `&client_secret=${CLIENT_SECRET}`;
    url += '&scope=user.info.basic,video.list';
    url += '&response_type=code';
    url += '&state=' + csrfState;
    url += `&redirect_uri=${SERVER_ENDPOINT_REDIRECT}`;

    res.status(200).json({ url });
});

app.get('/verify/tiktok', async (req, res, next) => {
    console.log(req);
    res.send(CLOSE_WINDOW);
});