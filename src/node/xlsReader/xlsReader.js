const xlsxFile = require("read-excel-file/node");

const finalArr = [];
const filedsMapping = [
  "id",
  "territory",
  "rightsSold",
  "language",
  "buyersName",
  "dateOfPurchase",
  "priorPeriod",
  "currentPeriod",
  "cumulativeSales",
];

function transformData(arr) {
  // Removing from the arr the first elemen if not id
  if (isNaN(arr[0])) filedsMapping.shift();

  // Both arrays must have the same size
  if (arr.length !== filedsMapping.length)
    throw new Error("Something fishy is happening!");

  const newMapping = {};
  for (i in filedsMapping) {
    // TODO: transforming the data
    newMapping[filedsMapping[i]] = arr[i];
  }

  finalArr.push(newMapping);
}

xlsxFile("./file.xlsm", { sheet: "Sales" }).then((rows) => {
  // Getting the header
  const head = rows.findIndex(
    (row) =>
      row.includes("Territory") ||
      row.includes("Rights Sold") ||
      row.includes("Language") ||
      row.includes("Date of Purchase")
  );

  for (let i = head + 1; i < rows.length; i++) {
    const nullCount = rows[i].reduce((acc, cur) => {
      return cur === null ? acc + 1 : acc;
    }, 0);

    // If it´s an empty row...
    if (nullCount >= 6) break;

    const isExampleRow = rows[i].findIndex(
      (elem) => elem === "e.g." || elem === "example"
    );

    // Discarding the example row, if exists...
    if (isExampleRow != -1) continue;

    transformData(rows[i]);
  }

  const productionRevenueRow = rows.findIndex((row) => {
    return (
      row.findIndex((elem) => {
        return elem && isNaN(elem) && elem.includes("(%)");
      }) > -1
    );
  });

  const [percent] = rows[productionRevenueRow].slice(-1);

  console.log({
    data: finalArr,
    percent,
  });
});
