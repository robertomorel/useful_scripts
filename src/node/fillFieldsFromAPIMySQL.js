/* eslint-disable no-console */
const mysql = require('mysql2/promise');
const { format } = require('date-fns');
const request = require('request-promise');

async function connect() {
    if (global.connection && global.connection.state !== 'disconnected') return global.connection;

    // mysql://user:password@server:port/db
    const connection = await mysql.createConnection('mysql://vc_client:WPy9mHRJfMU@localhost:3307/vc_client');
    console.log('MySQL connected!');
    global.connection = connection;
    return connection;
}

async function selectData() {
    const conn = await connect();
    const [rows] = await conn.query(`
        SELECT
            DISTINCT at2.project_id,
            fnps.id AS non_programming_id,
            JSON_EXTRACT(JSON_EXTRACT(JSON_EXTRACT(at2.settings, "$.pages"), '$[0]'), "$.id") AS page_id,
            at2.token AS page_token,
            fnps.period_of
        FROM facebook_non_programming_statistics fnps
        INNER JOIN projects p
            ON p.id = fnps.project_id
        INNER JOIN auth_tokens at2
            ON at2.project_id = fnps.project_id
        WHERE 1 = 1
        AND at2.settings is not null
        AND fnps.total_count is null
        AND at2.type = 'facebook'
        #AND fnps.period_of = '2020-12-12'
        #AND fnps.id in (126497)
        ORDER BY at2.project_id, fnps.period_of, page_id
        LIMIT 100;
    `);
    return rows;
}

async function queryFacebookAPI(uri, token) {
    if (!uri || !token) return;
    try {
        return await request.post(uri, {
            headers: [
                {
                    name: 'content-type',
                    value: 'application/x-www-form-urlencoded',
                },
            ],
            form: {
                access_token: token,
                method: 'GET',
            },
            json: true,
        });
    } catch (error) {
        console.error(`It wasn´t able to get Facebook data with URI = ${uri}; an token = ${token}`);
    }
}

async function queryOnAPI(element, pageId, token, iniDate, endDate) {
    const basePath = 'https://graph.facebook.com/v7.0/';

    // End of the first entrance
    const ini = `${format(iniDate, 'yyyy-MM-dd')}T23:59:59Z`;
    // Beginning of the final entrance
    const end = `${format(endDate, 'yyyy-MM-dd')}T23:59:59Z`;

    const uri = basePath + pageId + `/published_posts?summary=total_count&limit=100&since=${ini}&until=${end}`;

    const response = await queryFacebookAPI(uri, token);

    if (response?.data) {
        const totalCount = response?.data?.length || 0;
        await updateFacebookNonProgrammingStatistics(element.non_programming_id, totalCount);
        console.log(`Table updated with id = ${element.non_programming_id} and total_count = ${totalCount}`);
    } else {
        console.error(`It wasn´t able to update the table with id = ${element.non_programming_id}.`);
    }
}

async function updateFacebookNonProgrammingStatistics(id, value) {
    const conn = await connect();
    const sql = `UPDATE facebook_non_programming_statistics SET total_count = ${value} WHERE id = ${id};`;
    return await conn.query(sql);
}

(async () => {
    const data = await selectData();

    await data.map(async (element) => {
        try {
            // accessToken = await getAccessToken(element.page_id, element.page_token);
            const uri = `https://graph.facebook.com/v7.0/${element.page_id}?fields=id,access_token`;
            const result = await queryFacebookAPI(uri, element.page_token);
            if (result?.id) {
                const { id, access_token } = await queryFacebookAPI(uri, element.page_token);

                const previousDate = new Date(element.period_of.toString());
                previousDate.setDate(previousDate.getDate() - 1);
                const currDate = new Date(element.period_of.toString());

                await queryOnAPI(element, id, access_token, previousDate, currDate);
            }
        } catch (err) {
            console.error(err);
        }
    });
})();