const puppeteer = require('puppeteer');

const launchPage = async (uri) => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(uri);

    return [page, browser];
}

const closePage = async (browser) => await browser.close();

/**
 * Problem: Facebook can change this format any time
 */
(async () => {
    const [page, browser] = await launchPage('https://www.facebook.com/DITDdoc/');

    // Print a screenshot
    //await page.screenshot({path: 'facebook.png'});
    const result = await page.evaluate(() => {
        // This is the specific classname from the HTML DOM from the 'follow' <div>
        let elements = document.getElementsByClassName('_4bl9');
        for (let element of elements)
            // Find by this strict string
            if (element.textContent.includes(' people follow this'))
                return Number(element.textContent.replace(/\D/g, ""));
    })
    
    console.log(result);

    await closePage(browser);
})();


