// This module has code that is specific to the Pinterest API.
// It uses the got module to execute REST calls to api.pinterest.com.
var got = require("got");

// Construct the URI for the Pinterest OAuth 2.0 Authorization Endpoint.
exports.oauth_uri = function(req, { state = null }) {
  // Start with the required components of the URI
  var uri = `\
https://www.pinterest.com/oauth/?client_id=${req.session.app_id}\
&redirect_uri=https://${req.hostname}/callback\
&response_type=code\
&scope=${req.session.scope_string}`;

  // Add the state parameter, if requested.
  if (state) {
    uri += `&state=${state}`;
  }
  return uri;
};

// This function calculates the basic authorization required
// for the Pinterest API OAuth 2.0 token exchange.
exports.basic_auth = function(req) {
  const auth = '1473917:f024bf343aac27062fffe9c298a62e18e5311ac2';
  return Buffer.from(auth).toString("base64");
};

// This function performs the Pinterest API call that does
// the OAuth 2.0 token exchange, stores the results in the user
// session, and then executes the callback function.
exports.token_exchange = function(req, res, success, failure) {
  // The authorization code may only be used once. If it has
  // already been used, then skip the token exchange. This check
  // allows the page with the token to be refreshed in the browser.
  // if (!req.session.code) {
  //   failure("Token exchange attempted with no authorization code.");
  //   return;
  // }
  // if (req.session.used_code == req.session.code) {
  //   success(req, res);
  //   return;
  // }
  const post_headers = { Authorization: `Basic ${this.basic_auth()}` };
  const post_data = {
    code: '2064e33d36fba74086d7b0d3d5965b03cc2c288c',
    redirect_uri: `https://pinterest-oauth-tutorial.glitch.me/callback`,
    grant_type: "authorization_code"
  };

  // console.log('>>>', post_headers);
  // console.log('>>>', post_data);
  got
    .post("https://api.pinterest.com/v5/oauth/token", {
      headers: post_headers, // authentication as above
      form: post_data, // send body as x-www-form-urlencoded
      responseType: "json" // Pinterest API sends response as JSON
    })
    .then(postres => {
      console.log('>>>> ',  postres.body);
    })
    .catch(error => {
      failure(`Problem exchanging OAuth 2.0 code for token: ${error.message}`);
    });
};

// This function performs the GET request for the Pinterest API
// user_account endpoint, and then executes the callback function
// with an object containing the results of the GET.
exports.get_user_account = function(req, success, failure) {
  if (!req.session.access_token) {
    failure("Tutorial session is missing an access token.");
    return;
  }
  got
    .get("https://api.pinterest.com/v5/user_account", {
      headers: {
        Authorization: `Bearer ${req.session.access_token}`
      },
      responseType: "json" // Pinterest API sends response as JSON
    })
    .then(getres => {
      success({
        username: getres.body.username,
        account_type: getres.body.account_type,
        profile_image: getres.body.profile_image,
        website_url: getres.body.website_url
      });
    })
    .catch(error => {
      failure(`Problem fetching user account: ${error.message}`);
    });
};
