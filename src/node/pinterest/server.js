// This is the main module for the Pinterest OAuth 2.0 Tutorial.
// It configures the application framework, implements the application endpoints,
// and launches the service.
var express = require("express");
var session = require("express-session");
var multer = require("multer");
var path = require("path");
var url = require("url");
var uuid = require("uuid");

var api = require("./api"); // calls to the Pinterest API
var scopes = require("./scopes.js"); // Pinterest OAuth 2.0 scopes

var app = express(); // minimalist framework, suitable for this app

// Glitch uses a proxy, so this setting is required for session to use secure cookies.
app.set("trust proxy", 1);

// Configure application middleware...
app.use(express.static("public")); // directory with static assets

// Set up express-session.
app.use(
  session({
    secret: uuid.v4(), // cryptographically secure, suitable for single node app
    resave: false, // recommended setting
    saveUninitialized: false, // recommended setting
    cookie: { secure: true } // force HTTPS (requires "trust proxy," which is set above")
  })
);

app.use(multer().array()); // for posts of multipart/form-data

// Always use https. Redirect from http to https if necessary
// app.get("*", function(req, res, next) {
//   if (req.secure) {
//     return next();
//   }
//   res.redirect("https://" + "pinterest-oauth-tutorial.glitch.me" + req.url);
// });

// Generate the common parameters used to render EJS views.
function renderParameters(req) {
  return {
    app_id: '1473917',
    app_secret: 'f024bf343aac27062fffe9c298a62e18e5311ac2',
    basic_auth: api.basic_auth(),
    code: '15fda96b01ec8b222fd5de6a2cab90c0d78feed8',
    host: 'pinterest-oauth-tutorial.glitch.me',
    scopes: 'user_accounts:read,boards:read,boards:read_secret,pins:read,pins:read_secret,ads:read,boards:write,boards:write_secret,pins:write,pins:write_secret'
  };
}

// Application endpoints...

// Root/default endpoint for the app.
app.get("/", function(req, res) {
  if (req.headers["sec-fetch-dest"] == "iframe") {
    // In an iframe, so display a page that indicates how to get out
    // of the iframe in Glitch.
    res.render("00-app-in-window");
  } else {
    // Not in an iframe, so start with the tutorial introduction page.
    res.render("01-introduction");
  }
});

app.get("/cookie-explanation", function(req, res) {
  res.render("cookie-explanation");
});

// Provide instructions on getting a Pinterest app approved.
app.get("/requesting-trial-access", function(req, res) {
  res.render("02-requesting-trial-access");
});

// Get the Pinterest application identifier (id) and secret key
app.get("/app-credentials-get", function(req, res) {
  res.render("03-app-credentials-get", renderParameters(req));
});

// Receive the Pinterest application identifier (id) and secret key.
// Then, provide instructions on how to configure the redirect URI.
app.post("/redirect-uri", function(req, res) {
  if (!req.body || !req.body.app_id || !req.body.app_secret) {
    res.render("error-page", {
      error_content: "the page after entering app credentials",
      error_message: "Session does not contain app id and/or secret."
    });
    return;
  }
  req.session.app_id = req.body.app_id;
  req.session.app_secret = req.body.app_secret;
  req.session.save(function(err) {
    const parameters = renderParameters(req);
    res.render("04-redirect-uri", parameters);
  });
});

// A get request to this endpoint is not part of the normal
// tutorial flow. It supports a back-reference from
// subsequent information that indicates how to resolve
// issues with the oauth page.
app.get("/redirect-uri", function(req, res) {
  if (!req.session || !req.session.app_id || !req.session.app_secret) {
    res.render("error-page", {
      error_content: "the page after entering app credentials",
      error_message: "Session does not contain app id and/or secret."
    });
    return;
  }
  res.render("04-redirect-uri", renderParameters(req));
});

// Select Pinterest API OAuth 2.0 scopes.
app.get("/select-scopes", function(req, res) {
  res.render("05-select-scopes", renderParameters(req));
});

// Provide information about what needs an app should show
// on a login page, and how to start the OAuth 2.0 authorization
// flow on Pinterest.
app.post("/login-page", function(req, res) {
  // TODO: Need to verify that the app credentials are available
  // and go back to the app-credentials-get screen if they
  // are not.
  // TODO: Need to explain what happens if the app credentials
  // are wrong. (www.pinterest.com is supposed to throw a 4XX error
  // but does not seem to be doing that right now.)
  if (!req.session || !req.session.app_id || !req.session.app_secret) {
    res.render("error-page", {
      error_content: "the login page",
      error_message: "Session does not contain app id and/or secret."
    });
    return;
  }

  const parameters = renderParameters(req);
  var scope_string = "";
  var delimiter = "";
  for (const scope of scopes.oauth_scopes) {
    if (req.body[scope.name]) {
      scope_string += delimiter + scope.name;
      delimiter = ",";
    }
  }
  if (!scope_string) {
    res.render("error-page", {
      error_content: "the login page",
      error_message: "Scope string is missing."
    });
  }
  req.session.scope_string = scope_string;
  parameters.scope_string = scope_string;
  parameters.pinterest_oauth_uri = api.oauth_uri(req, {});
  req.session.save(function(err) {
    res.render("06-login-page", parameters);
  });
});

// Support get to the login-page endpoint, just for consistency.
app.get("/login-page", function(req, res) {
  if (
    !req.session ||
    !req.session.app_id ||
    !req.session.app_secret ||
    !req.session.scope_string
  ) {
    res.render("error-page", {
      error_content: "the login page",
      error_message: "Session does not contain required information."
    });
    return;
  }
  const parameters = renderParameters(req);
  parameters.scope_string = req.session.scope_string;
  parameters.pinterest_oauth_uri = api.oauth_uri(req, {});
  res.render("06-login-page", parameters);
});

// This endpoint is the OAuth 2.0 redirect URI configured
// on the developers.pinterest.com in a previous step. The
// primary purpose of this endpoint is to receive the authorization
// code from www.pinterest.com, which is transmitted as a URL parameter.
// Then, one of two things happens:
//   1. If the state parameter is empty, this is the first time through
//      the flow in the tutorial and the next step is to provide detailed
//      instructions on how to complete the OAuth flow.
//   2. If the state parameter is not empty, this is the second time through
//      the flow in the tutorial and the next step is to demonstrate how
//      to complete the flow within this server-side application.
app.get("/callback", function(req, res) {
  const urlparams = new url.URL(req.url, "http://example.com/").searchParams;
  // Save the authorization code.
  const code = urlparams.get("code");
  
  if (!code) {
    res.render("error-page", {
      error_content: "the authorization callback page",
      error_message: "The authorization code is missing."
    });
    return;
  }
  req.session.code = code;
  req.session.basic_auth = api.basic_auth(req);
  req.session.save(function(err) {
    const parameters = renderParameters(req);
    if (!urlparams.get("state")) {
      // First time: provide detailed instructions on exchanging
      res.render("07-first-callback", parameters);
    } else {
      // Second time: execute the token exchange and redirect in order to
      // complete the flow in this server. Redirect is better than just
      // rendering the page, because it ensures that the authorization code
      // never appears in the address bar of the user's browser.
      api.token_exchange(
        req,
        res,
        function(req, res) {
          res.redirect("/second-user-account");
        },
        function(error) {
          res.render("error-page", {
            error_content: "the token exchange callback",
            error_message: error
          });
        }
      );
    }
  });
});

// Perform the token exchange process and then provide instructions
// on how to use the access token to access the Pinterest API.
app.get("/exchange-token", function(req, res) {
  api.token_exchange(
    req,
    res,
    function(req, res) {
      const parameters = renderParameters(req);
      parameters.apiscopes = req.session.apiscopes;

      const atok = req.session.access_token;
      parameters.access_token = atok;
      // Truncate the token to make it easier to read, and to hide
      // enough of the value to keep it secure.
      parameters.atok_truncated = atok.slice(0, 20) + "[...]" + atok.slice(-20);
      res.render("08-exchange-token", parameters);
    },
    function(error) {
      if (error.match(/Unauthorized/)) {
        res.render("exchange-unauthorized", {
          error_message: error
        });
      } else {
        res.render("error-page", {
          error_content: "the token exchange page",
          error_message: error
        });
      }
    }
  );
});

// Execute the Pinterest API call (GET /v5/user_account), show the results,
// provide instructions on how to do the end-to-end flow so that it is
// transparent to users, and set up for the second OAuth 2.0 authorization request.
app.get("/first-user-account", function(req, res) {
  api.get_user_account(
    req,
    function(parameters) {
      Object.assign(parameters, renderParameters(req));
      // Note: setting state = user_flow triggers the second clause of the conditional
      // in the /callback endpoint, which executes the token exchange and redirects
      // to the /second-user-account endpoint.
      parameters.pinterest_oauth_uri = api.oauth_uri(req, {
        state: "user_flow"
      });
      res.render("09-first-user-account", parameters);
    },
    function(error) {
      res.render("error-page", {
        error_content:
          "the page with user information after the token exchange",
        error_message: error
      });
    }
  );
});

// Execute the Pinterest API call (GET /v5/user_account) as part of the end-to-end
// OAuth flow, and then show the results to the user.
app.get("/second-user-account", function(req, res) {
  api.get_user_account(
    req,
    function(parameters) {
      Object.assign(parameters, renderParameters(req));
      res.render("10-second-user-account", parameters);
    },
    function(error) {
      res.render("error-page", {
        error_content:
          "the page with user information after the OAuth redirect",
        error_message: error
      });
    }
  );
});

// Typical express application launch code for Glitch.
// Note that Glitch provides the https front-end as a proxy
// that calls this application on port 5000. That's why
// "trust proxy" needed to be enabled at the top of this file.
app.listen(5000, function(err) {
  if (err) return console.log(err);
  console.log("Listening at http://localhost:5000/");
});
