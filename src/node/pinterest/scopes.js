// This file encodes the Pinterest API OAuth 2.0 scopes documented at:
//   https://developers.pinterest.com/docs/api/v5/#section/Choosing-scopes-for-your-application
// In addition, it specifies the following:
//   * required: this scope is required for the tutorial to work (implies checked)
//   * checked: the tutorial enables this scope by default
exports.oauth_scopes = [
    {
      name: "user_accounts:read",
      description: "Read access to the user account information",
      required: true // This scope is required for the tutorial to work.
    },
    {
      name: "boards:read",
      description: "Read access to the user's public boards",
      checked: true // Check by default.
    },
    {
      name: "boards:read_secret",
      description: "Read access to the user’s secret boards"
    },
    {
      name: "pins:read",
      description: "Read access to the user's Pins",
      checked: true // Check by default.
    },
    {
      name: "pins:read_secret",
      description: "Read access to the user's secret Pins"
    },
    {
      name: "ads:read",
      description: "Read access to advertising data for the user"
    },
    {
      name: "boards:write",
      description: "Write access to create, update, or delete boards for the user"
    },
    {
      name: "boards:write_secret",
      description:
        "Write access to create, update, or delete secret boards for the user"
    },
    {
      name: "pins:write",
      description: "Write access to create, update, or delete Pins for the user"
    },
    {
      name: "pins:write_secret",
      description:
        "Write access to create, update, or delete secret Pins for the user"
    }
  ];
  