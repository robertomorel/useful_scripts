const OAuth = require('oauth')
const got = require('got')
const { promisify } = require('util')

const networkName = "twitter";
const redirectUri = "http://127.0.0.1:3000/api/networks/verify/twitter";
const requestTokenUrl = "https://api.twitter.com/oauth/request_token";
const accessTokenUrl = "https://api.twitter.com/oauth/access_token";
const usersShowUrl = "https://api.twitter.com/1.1/users/show.json";
const apiKey = "hzAAp2OmE00kAA3cxhF8MQeu8";
const apiSecret = "664N3uT6tHE42f9HYcdwI2XQmwnDQSxC6ETpxCGCzIg4zPP9bM";
const accessToken = "1172221473848614913-z4TpFlPp2ACWo7ICODFJYckPVjV6h1";
const accessSecret = "vrS7i8sjUNWHHFbIea6Bg1iW2DNtnSAYFSsBush2mwGzj";
const clientId = '1172221473848614913';

// const uri = `https://api.twitter.com/2/users/${clientId}?user.fields=public_metrics,created_at`;
const uri = `https://api.twitter.com/2/users/${clientId}/tweets?tweet.fields=attachments,author_id,created_at,id,public_metrics,source&exclude=replies,retweets&expansions=attachments.media_keys&media.fields=duration_ms,public_metrics,type&user.fields=created_at&max_results=5&start_time=2021-01-13T23:59:59Z&end_time=2021-01-15T23:59:59Z`;

getTwitterUserProfileWithOAuth2(clientId)
  .then((profile) => console.log('oauth2 response', JSON.stringify(profile, null, 2)) && process.exit(0))
  .catch(err => console.error(err) && process.exit(1))


async function getTwitterUserProfileWithOAuth2 (clientId = 'new') {
  var oauth2 = new OAuth.OAuth2(
    apiKey,
    apiSecret,
    'https://api.twitter.com/', null, 'oauth2/token', null
  )
  const getOAuthAccessToken = promisify(oauth2.getOAuthAccessToken.bind(oauth2));
  const accessToken = await getOAuthAccessToken('', { grant_type: 'client_credentials' });

  console.log('>> Token:', accessToken);

  return got(uri, {
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  })
    .then((res) => JSON.parse(res.body))
}