const request = require('request')
//const OAuth = require('oauth-1.0a')
const crypto = require('crypto')

const OAuth = require('oauth')
const { promisify } = require('util')

const networkName = "twitter";
const redirectUri = "http://127.0.0.1:3000/api/networks/verify/twitter";
const requestTokenUrl = "https://api.twitter.com/oauth/request_token";
const accessTokenUrl = "https://api.twitter.com/oauth/access_token";
const usersShowUrl = "https://api.twitter.com/1.1/users/show.json";
const apiKey = "hzAAp2OmE00kAA3cxhF8MQeu8";
const apiSecret = "664N3uT6tHE42f9HYcdwI2XQmwnDQSxC6ETpxCGCzIg4zPP9bM";
const accessToken = "1172221473848614913-z4TpFlPp2ACWo7ICODFJYckPVjV6h1";
const accessSecret = "vrS7i8sjUNWHHFbIea6Bg1iW2DNtnSAYFSsBush2mwGzj";

getTwitterUserProfileWithOAuth1('1135614012257906688')
  .then((profile) => console.log('oauth1 response', JSON.stringify(profile, null, 2)) && process.exit(0))
  .catch(err => console.error(err) && process.exit(1))

async function getTwitterUserProfileWithOAuth1 (username = '1135614012257906688') {
  var oauth = new OAuth.OAuth(
    requestTokenUrl,
    accessTokenUrl,
    apiKey,
    apiSecret,
    '1.0A', null, 'HMAC-SHA1'
  )
  const get = promisify(oauth.get.bind(oauth))

  const body = await get(
    `https://api.twitter.com/2/users/${username}?user.fields=public_metrics,created_at`,
    accessToken,
    accessSecret
  )

  return JSON.parse(body)
}