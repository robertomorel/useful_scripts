from services.request import create_url, connect_to_endpoint
from auth.auth import auth, create_headers
from mocks.mentions import payload
import json

if __name__ == "__main__":
    #Inputs for the request
    bearer_token = auth()
    headers = create_headers(bearer_token)
    keyword = ""
    start_time = "2021-03-01T00:00:00.000Z"
    end_time = "2021-03-31T00:00:00.000Z"
    max_results = 10

    url = create_url(keyword, start_time,end_time, max_results)
    #print('URL: ', url)
    #json_response = connect_to_endpoint(url[0], headers, url[1])
    json_response = json.loads(payload)
    #print('Response: ', json_response)

    # Retrieve the time from the first tweet was created
    print(json_response['data'][0]['text'])
    
    # Retrieve the next_token
    print(json_response['meta']['next_token'])

    # To save results in JSON. This must be used for next_token
    with open('src/files/data.json', 'w') as f:
        json.dump(json_response, f)

    # Saving data in the DB
        
