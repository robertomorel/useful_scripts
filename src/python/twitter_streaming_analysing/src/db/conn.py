import psycopg2

host = "localhost"
database="dw_local"
port=5432
user="postgres"
password="123123"

class postgres_connection():

    def create_connection(self):
        # Connection to database server
        conn = psycopg2.connect(host=host, database=database, port=port, user=user, password=password)
        return conn

    def close_connection(self, conn):
        conn.close()  

    def insert_data_in_db(self, data):    
        conn = self.create_connection()
        cur = conn.cursor()

        sql = "insert into cidade values (default,'São Paulo,'SP')"
        cur.execute(sql)
        conn.commit()

        recset = cur.fetchall()
        for rec in recset:
            print (rec)
        