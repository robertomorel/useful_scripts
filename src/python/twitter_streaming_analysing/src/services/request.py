#API: https://developer.twitter.com/en/docs/twitter-api/early-access
#API . Mentions: https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/api-reference/get-users-id-mentions#tab1

# For sending GET requests from the API
import requests

def create_url(keyword, start_date, end_date, max_results = 10):
    
    #search_url = "https://api.twitter.com/2/tweets/search/all"
    username = "gochampionsgo"
    search_url = f"https://api.twitter.com/2/users/by/username/{username}/mentions"


    # Change params based on the endpoint you are using
    query_params = {'query': keyword,
                    'start_time': start_date,
                    'end_time': end_date,
                    'max_results': max_results,
                    'expansions': 'attachments.poll_ids, attachments.media_keys, author_id, entities.mentions.username, geo.place_id, in_reply_to_user_id, referenced_tweets.id, referenced_tweets.id.author_id',
                    'tweet.fields': 'attachments, author_id, context_annotations, conversation_id, created_at, entities, geo, id, in_reply_to_user_id, lang, non_public_metrics, public_metrics, organic_metrics, promoted_metrics, possibly_sensitive, referenced_tweets, reply_settings, source, text, withheld',
                    'user.fields': 'created_at, description, entities, id, location, name, pinned_tweet_id, profile_image_url, protected, public_metrics, url, username, verified, withheld',
                    'place.fields': 'contained_within, country, country_code, full_name, geo, id, name, place_type',
                    'next_token': {}}
    return (search_url, query_params)

def connect_to_endpoint(url, headers, params, next_token = None):
    # params object received from create_url function
    params['next_token'] = next_token   
    response = requests.request("GET", url, headers = headers, params = params)

    print("Endpoint Response Code: " + str(response.status_code))

    if response.status_code != 200:
        raise Exception(response.status_code, response.text)
    return response.json()
    