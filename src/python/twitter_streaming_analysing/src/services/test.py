import sys
sys.path.append('src')
from auth import execute_authenticate

# Streaming tweets from home timeline
# By default, it returns 20 most recent tweets from your timeline but you can change that using the count parameter.
def tweets_from_home_timeline(api):
    public_tweet = api.home_timeline(count=5)

    for tweet in public_tweet:
        print("--> ", tweet.text)

# Streaming tweets from user timeline
# Accessing tweets from another user’s timeline
def tweets_from_user_timeline(api, user):
    public_tweet = api.user_timeline(id=user,count=5)

    for tweet in public_tweet:
        print("-->", tweet.text)  


def search_by_tweets(api):
    # Retrieve tweets
    result = api.search(['covid','Covid-19','COVID-19'], lang='en', count=10)

    # JSON keys
    pprint(result[0]._json.keys())  

    # Access hashtags from code
    pprint(result[4].entities['hashtags'])          

if __name__ == "__main__":
    api = execute_authenticate() 
    tweets_from_home_timeline(api)  
    #tweets_from_user_timeline(api, "AnalyticsVidhya")
    #search_by_tweets(api)