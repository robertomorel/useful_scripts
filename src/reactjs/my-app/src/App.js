import logo from './logo.png';
import './App.css';
import connectToNetwork from './functions/connectToNetwork';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} alt="logo" />
        <p>
          Click on the <b>link below</b> to start the linking process for TikTok.
        </p>
        <button className="App-button" onClick={() => connectToNetwork()}>
          TikTok
        </button>
      </header>
    </div>
  );
}

export default App;
