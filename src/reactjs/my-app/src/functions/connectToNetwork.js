import axios from 'axios';

const url = 'http://localhost:8080/oauth';

const openPopup = () => {
    const width = 800,
      height = 800;
    const left = window.innerWidth * 0.5 - width * 0.5;
    const top = window.innerHeight * 0.5 - height * 0.5;
    
    return axios
      .get(url)
      .then((res) => {
        const popup = window.open(
          res.data.url,
          'TikTok',
          `toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=${width}, height=${height}, top=${top}, left=${left}`,
        );

        if (!popup) {
          throw new Error('No popup has been rendered!!');
        }

        const timer = setInterval(function () {
          if (popup.closed) {
            clearInterval(timer);
            console.log('HERE!!!!!')
          }
        }, 1000);
      })
      .catch((error) => {
        const errorMessage =
          (error.response && ((error.response.data && error.response.data.error) || error.response.statusText)) || error.message;

        throw new Error(errorMessage);
      });

};

const connectToNetwork = (event) => {
    if (event) event.preventDefault();
    openPopup();
};
  
  export default connectToNetwork;